/* 
Connect from the command line to gateway server 'gateway_server.js'
$ node client_nw.js

You can pass this args:
-p <server port> (50000)
-h <IP server> (127.0.0.1)
-t <customize message> ()
-r <number of request> (1000)
-d <delay between request> (1000)

 */

/* *************  Script arguments  ********************** */
var opts = new (require('./script_args'))();
opts.opt_add('p', 'server port', 50000, _parseInt);
opts.opt_add('h', 'IP server', '127.0.0.1');
opts.opt_add('t', 'customize message', '');
opts.opt_add('r', 'number of request', 1000);
opts.opt_add('d', 'delay between request (ms)', 1000);

function _parseInt(x, d) {var r = parseInt(x,10); return r>1024 && r || d;}

if (!opts.process_argv(process.argv)) return;

var opt = opts.opt;
var _TEXT = opt.t[1] + ' No Wait Client sending Hello Echo server!'
var _DELAY = opt.d[1];
var _REQS = opt.r[1];
var _PORT = opt.p[1];
var _HOST = opt.h[1];

var _cTEXT = _TEXT.slice(0,40)

var _EXIT = false;

console.log(_HOST,_PORT);

var _Scounter = 0;
var _Rcounter = 0;

var net = require('net');

var client = new net.Socket();

client.connect(_PORT, _HOST, function() {
  console.log('Connected to ', client.remoteAddress + ':' + client.remotePort);
  setTimeout(sendData,_DELAY);
});

client.on('data', function(data) {
  if (data.toString().slice(0,25)!='Welcome to Gateway server' && data.toString().slice(0,40)!=_cTEXT) {
    console.log('Bad Received: ' + _cad);
    process.exit(1);
  }
  _Rcounter++;
  console.log('Received: ' + data);
});

client.on('end', function() {
  console.log('Client ended.');
});

client.on('close', function() {
  _EXIT = true;
  console.log('Client closed.');
});

client.on('error', function(err) {
  console.log('Client error:', err.message);
});

function sendData() {
  if (!_EXIT) {
    var _cad = _TEXT + ' ' + (++_Scounter).toString();
    console.log('Sending : ' + _cad + ' :: ' + _Rcounter.toString());
    client.write(_cad);
    if (_Scounter< _REQS) setTimeout(sendData,_DELAY);
    //setTimeout(sendData,_TEMP);
  } else client.end();
}


process.on('SIGINT', function() {
  _EXIT = true;
  client.end()
});
