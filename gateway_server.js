/*
Start gateway tcp server
$ node gateway_server.js

You can pass this args:
-p <listening port> (50000)
-tp <target port> (50001)
-th <target IP> (127.0.0.1)

Ex: Connects to a target server in 192.168.55.1 using port 50001 
and clients connects to this server using port 50000
$ node gateway_server.js -th 192.168.55.1

Ex: Connects to a target server in 192.168.55.1 using port 50001 
and clients connects to this server using port 49999
$ node gateway_server.js -p 49999

*/

/* *************  Script arguments  ********************** */
var opts = new (require('./script_args'))();
opts.opt_add('p', 'listening port', 50000, _parseInt);
opts.opt_add('tp', 'target port', 50001, _parseInt);
opts.opt_add('th', 'target IP','127.0.0.1');

function _parseInt(x, d) {var r = parseInt(x,10); return r>1024 && r || d;}

if (!opts.process_argv(process.argv)) return;

var opt = opts.opt;
var _tPORT = opt.tp[1];
var _tHOST = opt.th[1];
var _PORT = opt.p[1];
var _HOST = '0.0.0.0'


var net = require('net');

/* *************  Target Server  ********************** */
var target = new net.Socket();

target.connect(_tPORT, _tHOST, function() {
  console.log('Connected to target Server ', target.remoteAddress + ':' + target.remotePort);
});

target.on('data', function(data) {
  console.log('Received from target: ' + data);
  var s = queue._currentSocket;
  queue.process(null, true);
  if (s) s.write(data);
});

target.on('end', function() {
  console.log('Target ended.');
});

target.on('close', function() {
  console.log('Target closed.');
  proc_exit();
});

target.on('error', function(err) {
  console.log('Target error:', err.message);
});


/* *************  Gateway Server  ********************** */
var clients = [];

var bridge = net.createServer();

bridge.on('connection', function(socket) {
  var myRA = socket.remoteAddress;
  console.log('New connection from ' + myRA);
  clients.push(socket);
  socket.q = [];
  socket.write('Welcome to Gateway server ' + myRA);
  
  socket.on('data', function(data) {
    console.log('got data from ' + myRA + ' :: ' + data);
    socket.q.push(data);
    queue.process(socket,null)
  });
  
  socket.on('end', function() {
    console.log(myRA + ' client disconnected.');
  });
  
  socket.on('error', function(err) {
    console.log(myRA + ' error:', err.message);
  });
  
  socket.on('close', function(has_err) {
    msg = myRA;
    clients.splice(clients.indexOf(socket), 1);
    queue.clear(socket);
    if (has_err) msg += ' Closed with errors.'; else msg += ' Closed.';
    console.log(msg);
  });
  
});

bridge.on('listening', function() {
  console.log('Gateway Server listening:', bridge.address());
});

bridge.on('error', function(err) {
  console.log('Gateway Server error:', err.message);
});

bridge.on('close', function() {
  console.log('Gateway Server closed.');
});

bridge.listen(_PORT, _HOST);


/* *************  Request Queue  ********************** */
var _queue = function () {
  this.queue = [];
  this._ready = true;
  this._currentSocket = null;
}

_queue.prototype.clear = function(s) {
    var q = this.queue;
    for (var i = q.length-1; i>=0; i--)
      if (q[i]==s) q.splice(i,1);
}

_queue.prototype.clearALT = function(s) {
    var q = this.queue;
    for (var i = q.length-1; i>=0; i--) 
      if (q[i]==s) q[i] = null;
}

_queue.prototype.process = function (s, ready) {
    if (ready) this._ready = true;

    var q = this.queue;
    
    if (s) q.push(s);
    //console.log(q.length);
    
    if (!this._ready) return;

    var n = q.shift();
    if (!n) return;

    this._ready = false;
    this._currentSocket = n;

    target.write(n.q.shift());
}

_queue.prototype.processALT = function (s, ready) {
    if (ready) this._ready = true;

    var q = this.queue;
    
    if (s) q.push(s);
    //console.log(q.length);
    
    if (!this._ready) return;

    var ql = q.length;
    for (var i=0;i<ql;i++)
        if (q[i]) break;
    q.splice(0,i);
    
    var n = q.shift();
    if (!n) return;

    this._ready = false;
    this._currentSocket = n;

    target.write(n.q.shift());
}

var queue = new _queue();


/* *************  Exit functions  ********************** */
process.on('SIGINT', function() {
  proc_exit();
});

function proc_exit() {
  target.end();
  bridge.close();
  //for (var i in clients) clients[i].destroy();
  for (var i in clients) clients[i].end();
  //process.exit(1);
}

