
var SARGS = function () {
  this.opt = {};
}

SARGS.prototype.process_argv = function (args) {
  args = args.slice(2);
  if (args.length%2) {
    console.log('Incorrect arguments number');
    console.log(this.opt_text());
    return false;
  }
  var cargs = [];
  while (args.length) cargs.push(args.splice(0,2));
  var opt = this.opt;
  for (i in cargs) {
    var o = cargs[i][0].slice(1);
    if (opt[o]) {
      opt[o][1] = opt[o][2](cargs[i][1], opt[o][1]);
    } else {
      console.log('Incorrect argument', cargs[i][0]);
      console.log(this.opt_text());
      return;
    }
  }
  return true;
}

SARGS.prototype.opt_text = function () {
  var c = '';
  var opt = this.opt;
  for (i in opt)
    c += '-'+ i + ' <' + opt[i][0] + '> (' + opt[i][1].toString() + ')\n';
  return c;
}

SARGS.prototype.opt_add = function (o, desc, def, func) {
  if (!func) {
    if (typeof(def)=='number') func = _parseInt;
    else func = _toString;
  }
  var opt = this.opt;
  opt[o] = [desc,def,func];
}
function _parseInt(x, d) {var r = parseInt(x,10); if (isNaN(r)) r=d; return r;}
function _toString(x, d) {return x.toString() || d;}


module.exports = SARGS;
