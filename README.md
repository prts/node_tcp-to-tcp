tcp-to-tcp
=


A tcp server (gateway) that captures clients requests and redirec them to another tcp server (target) one by one, sequentially ...   

Thanks to all folks writing and sharing a similar code.


### How it works
The gateway accepts and stores client requests. Then FIFO them to target server.   
The gateway receive the response from target and redirect them to the corresponding client.   
The target server is only capable of attending one petition at a time and acts like a echo server.   
Clients connect to gateway server and this is the only one connected to target server.   



### Target Server
Start the target tcp echo server
```bash
$ node target_server.js
```

### Gateway Server
Start the gateway tcp server
```bash
$ node gateway_server.js
```

### Client
Connect from the command line with the tcp client 'client.js' or 'client_nw.js' or use netcat.
```bash
$ node client.js -t cli
$ node client_nw.js -t cli2 -r 100 -d 500
$ netcat 127.0.0.1 50000
```


'client.js' sends requests but after receive response of previous plus a timeout.   
'client_nw.js' sends all requests without waiting any response, but with a timeout.


