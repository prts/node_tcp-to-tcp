/* 
Connect from the command line to gateway server 'gateway_server.js'
$ node client.js

You can pass this args:
-p <server port> (50000)
-h <IP server> (127.0.0.1)
-t <customize message> ()
-d <delay between request> (1000)

 */

/* *************  Script arguments  ********************** */
var opts = new (require('./script_args'))();
opts.opt_add('p', 'server port', 50000, _parseInt);
opts.opt_add('h', 'IP server', '127.0.0.1');
opts.opt_add('t', 'customize message', '');
opts.opt_add('d', 'delay between request (ms)', 1000);

function _parseInt(x, d) {var r = parseInt(x,10); return r>1024 && r || d;}

if (!opts.process_argv(process.argv)) return;

var opt = opts.opt;
var _TEXT = opt.t[1] + ' Client sending Hello Echo server!'
var _DELAY = opt.d[1];
var _PORT = opt.p[1];
var _HOST = opt.h[1];

var _EXIT = false;
var _counter = 0;

var net = require('net');

var client = new net.Socket();

client.connect(_PORT, _HOST, function() {
  console.log('Connected to ', client.remoteAddress + ':' + client.remotePort);
});

client.on('data', function(data) {
  console.log('Received: ' + data);
  var _cad = _TEXT + ' ' + _counter.toString();
  if (data.toString().slice(0,25)!='Welcome to Gateway server' && data.toString()!=_cad) {
    console.log('Bad Received: ' + _cad);
    process.exit(1);
  }
  if (!_EXIT) {
    var _cad = _TEXT + ' ' + (++_counter).toString();
    setTimeout(function() {
      console.log('Sending : ' + _cad);
      client.write(_cad);
    },_DELAY);
    //console.log('Sending : ' + _TEXT)
    //client.write(_TEXT);
  } else client.end();
});

client.on('end', function() {
  console.log('Client ended.');
});

client.on('close', function() {
  console.log('Client closed.');
});

client.on('error', function(err) {
  console.log('Client error:', err.message);
});

process.on('SIGINT', function() {
  _EXIT = true;
});
