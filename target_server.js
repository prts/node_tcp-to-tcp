/*
Start target tcp echo server
$ node target_server.js

You can pass the listening port (defaults to 50001)
$ node target_server.js 50001

*/

var p = parseInt((process.argv[2] || '0'));
var _PORT = p>1024 && p || 50001;
var _HOST = '0.0.0.0';
var _DELAY = 500;

var net = require('net');
var _client = false;

var server = net.createServer();

server.on('connection', function(socket) {
  if (_client) {
    console.log('I have already a client ...');
    socket.write('I have already a client ...');
    socket.destroy();
    return
  }
  _client = socket;
  var myRA = socket.remoteAddress;
  console.log('New connection from ' + myRA);
  socket.write('Welcome to Echo server ' + myRA);
  
  socket.on('data', function(data) {
    console.log('got data ' + data);
    //socket.write(data);
    setTimeout(function(){
      socket.write(data)
    },_DELAY);
  });
  
  socket.on('end', function() {
    console.log(myRA + ' client disconnected.');
  });
  
  socket.on('error', function(err) {
    console.log(myRA + ' error:');
    console.log(err.message);
  });
  
  socket.on('close', function(has_err) {
    msg = myRA;
    _client = false;
    if (has_err) msg += ' Closed with errors.'; else msg += ' Closed.';
    console.log(msg);
  });
  
});

server.on('listening', function() {
  console.log('Echo Server listening:', server.address());
});

server.on('error', function(err) {
  console.log('Server error:', err.message);
});
server.on('close', function() {
  console.log('Server closed.');
});

server.listen(_PORT, _HOST);

process.on('SIGINT', function() {
  server.close();
  if (_client) _client.end();
  //if (_client) _client.destroy();
  //process.exit(1);
});
